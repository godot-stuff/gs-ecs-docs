.. _Group:

Group
======
Inherits: Node_

A Group is a way to organize :ref:`Systems<system>`.

Description
+++++++++++

Sometimes it is very useful to put similar Systems together in a Group so that they can be processed or not processed together. This is sometimes more simple than removing a :ref:`component` from an Entity you want to control processing on.

In other ECS frameworks, this is sometimes refered to as a World.

How It Works
++++++++++++
To use it, you simply add a Group node to your Scene, and attach Systems to that Node. The example below shows you how that might look.

.. image:: images/groups.png

In the Scene we have two Groups named **RotatingGroup** and **MovingGroup**, and each has a System attached to it respectively. When your game now runs, you can choose to :ref:`update<update>` all Systems, or you can control which Systems to process based upon the Group they are in.

Then, when we want to :ref:`update<update>` only one specific :ref:`group` of :ref:`Systems<system>` we can do this

.. code:: gdscript

    func _process(delta):
        ECS.update('rotatingsystem')



.. _int: https://docs.godotengine.org/en/stable/classes/class_int.html#class-int
.. _bool: https://docs.godotengine.org/en/stable/classes/class_bool.html#class-bool
.. _node: https://docs.godotengine.org/en/stable/classes/class_node.html?highlight=node
.. _String: https://docs.godotengine.org/en/stable/classes/class_string.html
