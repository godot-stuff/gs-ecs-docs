.. _Component:

Component
=========

Inherits: Node_

A Component contains the Data for an Entity in the ECS Framework. 

Description
+++++++++++

An entity is essentially a Node in Godot. In fact it is a base class of a Godot Node.
In this **Framework** a Component is nothing more than a Script that is attached to a Node. The Script defines the data for the Component.

Components can be added to an Entity programatically or by using the Node Structure of an Entity by attaching any Component Node in the Tree.

In the example below the Entity called **TestEntity** has three **Components** attached to it called **Movable**, **Rotating** and **Energy** respectively. The node called "Components" is used to keep the structure of the Entity node clean and gives a good visual representation for the developer.

.. image:: images/components.png

Methods
+++++++
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_init_ ()                                                                 |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_ready_ ()                                                                 |
+-----------------------+-------------------------------------------------------------------------------+

Method Descriptions 
+++++++++++++++++++

.. _on_init:

* void **on_init** ()

The framework will make a virtual call when the Entity is initialized. Use this in place of _init().

.. _on_ready:

* void **on_ready** ()

The framework will make this virtual call when the Entity is ready. Use this in place of _ready().

.. _node: https://docs.godotengine.org/en/stable/classes/class_node.html?highlight=node
