gs-ecs -- *Master* branch
=========================

.. tip::

    This is documentation for the development (master) branch. Looking for documentation
    of the current stable branch? Have a look here.

Welcome to the official documentation of the gs-ecs library. A lightweight Entity 
Component System for the Godot Engine.

The table of contents below and in the sidebar should let you easily 
access the documentation for your topic of interest. You can also use
the search function in the top left corner.

.. note::

    gs-ecs is an open source project.

    Submit an issue or pull request on the GitLab_ repository.

.. toctree::
    :maxdepth: 1
    :caption: General

    About <about.rst>


.. toctree::
    :maxdepth: 1
    :caption: Getting Started

    Installation <install.rst>
    Activation <activate.rst>


.. toctree::
    :maxdepth: 1
    :caption: Tutorials

    A Simple Example <simple.rst>

.. toctree::
    :maxdepth: 1
    :caption: Singletons

    Singletons <singletons.rst>


.. toctree::
    :maxdepth: 2
    :caption: Class Reference

    Classes <classes.rst>

.. _gitlab: https://gitlab.com/godot-stuff/gs-ecs
