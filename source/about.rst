About
=====
This is a **Framework** for adding a simple **Entity Component System** using **Godot**.

The **Framework** puts less emphasis on performance, and instead tries to focus on improving **Workflow** and **Code Reuse**.
