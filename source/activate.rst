Activation
==========

The main ECS library, along with it's supporting library Logger, must be activated to make sure that the proper Autoload objects are included in your Project.

- Open your Project Settings and open the Plugins tab
- Activate the Logger and ECS Plugins

.. image:: images/plugins.png

After that is complete, you should see something similar to this in your Autoload

.. image:: images/autoload.png

.. note::

    The Logger autoload needs to come before the ECS autoload in order to work properly.

