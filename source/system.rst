.. _System:

System
======
Inherits: Node_

A :ref:`system` is used to make the **things** in your Game go.

Description
+++++++++++

A :ref:`system` will process entities that contain, or do not contain, a certain type of :ref:`component`.

For example, say your game needs to be able to move an :ref:`entity` across the scene. You would need some kind of :ref:`component` that stores the direction and speed of an Entity, let's call it "Velocity". To move the :ref:`entity`, we would create a :ref:`system` that would only run over Entities that have the "velocity" :ref:`component`, and then calculate the velocity.

You might then create another :ref:`system` that uses Entities that have a "velocity" :ref:`component` to move them to the next position in the Scene based on the Velocity that was previously calculated, and so on.

You can make your :ref:`system` as simple or complicated as needed, but we have found that breaking up your game into smaller, logical domains or behaviors makes development of your game easier, without any type of large performance hit.

.. note::

    In some cases it does not make much sense to use a :ref:`system`. If you are, for example, creating a bullet-hell type of game, it might be more efficient to use the standard Godot processing loop to move those. However, the Collision Checking and Spawner for the Bullets might be created with a :ref:`system`. The Framework is flexible enough to let you decide the best place to use it.

.. note::

    We suggest that you check out the :ref:`simple` example project to get a better understanding of how everything works together. It should help you understand where a :ref:`system` fits into the Framework.

Properties
++++++++++

+---------------+---------------------------+
|   bool_       |   enabled_                |
+---------------+---------------------------+
|   String_     |   components_             |
+---------------+---------------------------+

Methods
+++++++

+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_init_ ()                                                                 |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_ready_ ()                                                                |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_before_add_ ()                                                           |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_after_add_ ()                                                            |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_before_remove_ ()                                                        |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_after_remove_ ()                                                         |
+-----------------------+-------------------------------------------------------------------------------+

Property Descriptions 
+++++++++++++++++++++

.. _enabled:

* bool_ **enabled**

Lets you enable the entity by setting this to True. When an Entity is not enabled when its setting is False, it will not be included during System updates.

.. _components:

* String_ **components**

A very simple comma separated list of :ref:`component` names to be processed when the updated.

The syntax is

+-----------------------+-------------------------------------------------------------------------------+
|   {component}         |   any :ref:`component` matching this name                                     |
+-----------------------+-------------------------------------------------------------------------------+
|   !{component}        |   any :ref:`component` not matching this name                                 |
+-----------------------+-------------------------------------------------------------------------------+

In the example below, the :ref:`system` will process all Entities that have a Velocity and Move :ref:`component`, but not an Enemy component.

.. code::

    velocity, move, !enemy

You can treat each comma as an AND statement for each :ref:`component`. So another way to read the above is

.. code::

    **velocity** and *movement** and not **enemy**

Method Descriptions 
+++++++++++++++++++

.. _on_init:

* void **on_init** ()

The framework provides this in place of the _init() call.

.. _on_ready:

* void **on_ready** ()

The framework provides this in place of the _ready() virtual call.

.. _on_before_add:

* void **on_before_add** ()

The framework will make this virtual call just before the :ref:`system` is registered.

.. _on_after_add:

* void **on_after_add** ()

The framework will make this virtual call just after the :ref:`system` has been registered.

.. _on_after_remove:

* void **on_after_remove** ()

The framework will make this virtual call just after the :ref:`system` is removed.

.. _on_before_remove:

* void **on_before_remove** ()

The framework will make this virtual call just before the :ref:`system` is removed.

.. _int: https://docs.godotengine.org/en/stable/classes/class_int.html#class-int
.. _bool: https://docs.godotengine.org/en/stable/classes/class_bool.html#class-bool
.. _node: https://docs.godotengine.org/en/stable/classes/class_node.html?highlight=node
.. _String: https://docs.godotengine.org/en/stable/classes/class_string.html
