.. _ECS:

ECS
===

Inherits: Node_

A singleton for managing the simple `Entity Component System <https://en.wikipedia.org/wiki/Entity_component_system>`_ for Godot.

Description
+++++++++++

This framework does not try to add a lot of functionality, instead it tries to provide a low level API for managing objects for an ECS like environment. 


Inherits: Node

Properties
++++++++++

+---------------+---------------------------+
|   Dictionary_ |   entities_               |
+---------------+---------------------------+
|   Dictionary_ |   component_entities_     |
+---------------+---------------------------+
|   Dictionary_ |   systems_                |
+---------------+---------------------------+
|   Dictionary_ |   system_components_      |
+---------------+---------------------------+
|   Dictionary_ |   system_entities_        |
+---------------+---------------------------+
|   Dictionary_ |   groups_                 |
+---------------+---------------------------+
|   Dictionary_ |   group_systems_          |
+---------------+---------------------------+
|   Dictionary_ |   entity_remove_queue_    |
+---------------+---------------------------+
|   bool_       |   is_dirty_               |
+---------------+---------------------------+

Methods
+++++++

+-----------------------+-------------------------------------------------------------------------------+
|   void                |   add_component_ ( :ref:`component` component )                               |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   add_entity_ ( :ref:`entity` entity )                                        |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   add_group_ ( :ref:`group` group )                                           |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   add_system_ ( :ref:`system` system )                                        |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   clean_ ( )                                                                  |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   entity_add_component_ ( :ref:`entity` entity, :ref:`component` component )  |
+-----------------------+-------------------------------------------------------------------------------+
|   :ref:`component`    |   entity_get_component_ ( :ref:`entity` entity, String_ name )                |
+-----------------------+-------------------------------------------------------------------------------+
|   bool_               |   entity_has_component_ ( :ref:`entity` entity, String_ name )                |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   entity_remove_component_ ( :ref:`entity` entity, String_ name )             |
+-----------------------+-------------------------------------------------------------------------------+
|   Dictionary_         |   get_all_components_ ( )                                                     |
+-----------------------+-------------------------------------------------------------------------------+
|   :ref:`component`    |   get_component_ ( String_ name )                                             |
+-----------------------+-------------------------------------------------------------------------------+
|   bool_               |   has_component_ ( String_ name )                                             |
+-----------------------+-------------------------------------------------------------------------------+
|   bool_               |   has_entity_ ( String_ name )                                                |
+-----------------------+-------------------------------------------------------------------------------+
|   bool_               |   has_group_ ( String_ name )                                                 |
+-----------------------+-------------------------------------------------------------------------------+
|   bool_               |   has_system_ ( String_ name )                                                |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   rebuild_ ( )                                                                |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   remove_component_ ( String_ name )                                          |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   remove_entity_ ( String_ name )                                             |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   remove_group_ ( String_ name )                                              |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   remove_system_ ( String_ name )                                             |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   update_ ( String_ group = :code:`null`,  float_ delta = :code:`null` )      |
+-----------------------+-------------------------------------------------------------------------------+


Property Descriptions 
+++++++++++++++++++++

.. _entities:

* Dictionary_ **entities**

A list containing all the entities that have been registered.


.. _component_entities:

* Dictionary_ **component_entities**

A list containing all the entities with a component that have been registered.


.. _systems:

* Dictionary_ **systems**

A list containing all the systems that have been registered.


.. _system_components:

* Dictionary_ **system_components**

A list containing all the components in a system that have been registered.


.. _system_entities:

* Dictionary_ **system_entities**

A list containing all the entities in a system.


.. _groups:

* Dictionary_ **groups**

A list containing all the groups in the framework.


.. _group_systems:

* Dictionary_ **group_systems**

A list of systems in a group.


.. _entity_remove_queue:

* Dictionary_ **entity_remove_queue**

A list of entities to remove after all systems have completed running.


.. _is_dirty:

* bool_ **is_dirty**

Returns :code:`true` when the framework needs to be cleaned up. This usually occurs when any objects have been added or removed in the framework. You can set this property to true at any time to force a rebuild of the systems.


Method Descriptions
+++++++++++++++++++

.. _add_component:

* void **add_component** ( :ref:`component` component )

Adds a :ref:`component` to the framework. If the component already exists, it will not be added again.


.. _add_entity:

* void **add_entity** ( :ref:`entity` entity )

Adds an :ref:`entity` to the framework. If the entity has already been added, it will not be added again.


.. _add_group:

* void **add_group** ( :ref:`group` system )

Adds a :ref:`group` to the framework. If the group has already been added, it will not be added again.


.. _add_system:

* void **add_system** ( :ref:`system` system )

Adds a :ref:`system` to the framework. If the system has already been added, it will not be added again.


.. _clean:

* void **clean** ( )

Removes all odbjects from the framework.


.. _entity_add_component:

* void **entity_add_component** ( :ref:`entity` entity, :ref:`component` component )

For a given :ref:`entity`, add a :ref:`component` to it.


.. _entity_get_component:

* :ref:`component` **entity_get_component** ( :ref:`entity` entity, String_ name )

For a given :ref:`entity`, return a :ref:`component` from it based on the name given.


.. _entity_has_component:

* bool_ **entity_has_component** ( :ref:`entity` entity, String_ name )

For a given :ref:`entity`, returns :code:`true` if the named component is available.


.. _entity_remove_component:

* bool_ **entity_remove_component** ( :ref:`entity` entity, String_ name )

For a given :ref:`entity`, remove the named component.


.. _get_all_components:

* Dictionary_ **get_all_components** ( )

Returns all components in the framework.


.. _get_component:

* :ref:`component` **get_component** ( String_ name )

Returns a :ref:`component` from the framework in :code:`name`


.. _has_component:

* bool_ **has_component** ( String_ name )

Returns :code:`true` if the :ref:`component` in :code:`name` exists.


.. _has_entity:

* bool_ **has_entity** ( String_ name )

Returns :code:`true` if the :ref:`entity` in :code:`name` exists.


.. _has_group:

* bool_ **has_group** ( String_ name )

Returns :code:`true` if the :ref:`group` in :code:`name` exists.


.. _has_system:

* bool_ **has_system** ( String_ name )

Returns :code:`true` if the :ref:`system` in :code:`name` exists.


.. _rebuild:

* bool_ **rebuild** ( )

Rebuild the system_entities_.


.. _remove_component:

* void **remove_component** ( String_ name )

Rmoves the :ref:`component` from the framework in :code:`name`.


.. _remove_entity:

* void **remove_entity** ( String_ name )

Rmoves the :ref:`entity` from the framework in :code:`name`.


.. _remove_group:

* void **remove_group** ( String_ name )

Rmoves the :ref:`group` from the framework in :code:`name`.


.. _remove_system:

* void **remove_system** ( String_ name )

Rmoves the :ref:`system` from the framework in :code:`name`.


.. _update:

* bool_ **update** ( String_ group = :code:`null`,  float_ delta = :code:`null` )

Update the systems_ in the framework. A :ref:`group` may be specified in addition to a :code:`delta`.


.. _Array: https://docs.godotengine.org/en/stable/classes/class_array.html
.. _int: https://docs.godotengine.org/en/stable/classes/class_int.html#class-int
.. _bool: https://docs.godotengine.org/en/stable/classes/class_bool.html#class-bool
.. _String: https://docs.godotengine.org/en/stable/classes/class_string.html
.. _Dictionary: https://docs.godotengine.org/en/stable/classes/class_dictionary.html
.. _float: https://docs.godotengine.org/en/stable/classes/class_float.html#class-float
.. _node: https://docs.godotengine.org/en/stable/classes/class_node.html?highlight=node
