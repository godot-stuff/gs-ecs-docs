Your First ECS Game
===================

Overview
++++++++

This Tutorial will show you how to convert the "Dodge The Creeps" tutorial in the Godot documentation, and show you how to build it using the ECS framework.

.. note::

    This is a complete re-write of the original tutorial using the ECS framework. 

Project setup
+++++++++++++

Launch Godot and create a new project. Then, download
:download:`dodge_assets.zip <files/dodge_assets.zip>`. This contains the images and sounds you'll be using to make the game. Unzip these files in your project folder. Then, download . This contains the ECS framework and Logging framework for the project.

This game is designed for portrait mode, so we need to adjust the size of the game window. Click on Project -> Project Settings -> Display -> Window and set "Width" to ``480`` and "Height" to ``720``.

Also in this section, under the "Stretch" options, set ``Mode`` to "2d" and ``Aspect`` to "keep". This ensures that the game scales consistently on different sized screens.

ECS Recipe
++++++++++

In the dish we call ECS, the first ingredient is the Entity. Most of the time, these are pretty easy to identify, like in the game we are making. We have a Player and a Mob. There is also one additional Entity, the Game itself. Anytime you have something that has a behaviour, like spawing enemies, as is the case of our Game, we can make it an Entity.

So we start with our list of Entities that our game needs.

+---------------+---------------------------------------------------------+
|   Player      |   The Character you control in the game                 |
+---------------+---------------------------------------------------------+
|   Mob         |   The Character(s) you are trying to avoid in the game  |
+---------------+---------------------------------------------------------+
|   Game        |   Keeps track of scoring and spawns mobs, etc           |
+---------------+---------------------------------------------------------+

The entities will each have rules, sometimes called logic, that will end up defining the Components and Systems that we will need later on. When you are building your game, it is okay of you don't have all the rules at the beginning, just start with what you know and you can add to it later.

Let's see what our Entities can do.

Player
    - can move in eight directions
    - cannot move outside screen area
    - can collide with mob
    - can animate
    - will be controlled

Mob
    - will move in one of eight directions
    - can collide with player
    - will be removed when offscreen
    - will spawn in a random offscreen location
    - can animate

Game
    - can spawn mobs
    - can keep score


Now that we have our Entites, and we know our rules, we can start coming up with the Components that will be used. When building using the ECS framework, these will hold the data we need to create our rules. It is not a one to one relationship, and the more you can abstract the data, the better.

Components
    - score - has a score and hi score
    - spawn - it can spawn somewhere at a certain interval
    - velocity - contains a direction and speed
    - collide - it can hit other entities
    - timer - it will activate after a certain length of time
    - control - it is controllable
    - speed - a minimum and maximum speed
    - player - the player
    - mob - is a mob
    - game - is a game
    - destroy - is to be destroyed

The last ingredient will be the definition of our Systems that will drive the behaviours and logic for the game. These systems will contains almost all of the code necessary for the game. As with your Components, it is alright if you don't get them all figured out at once. Just focus on the ones you know you will need for sure.

Common
    - Moving - continually updates an entity moving with a velocity
    - Spawn - spawn an entity in the game
    - Bound - will bind an entity inside the screen
    - Offscreen - will flag an entity for destruction when it is offscreen
    - Destroy - destroys an entity

Player
    - PlayerCollide - handles player collisions
    - PlayerControl - handles input for the player

Mob
    - MobCollide - checks for mob collisions
    - RobotControl - the ai of the robot
    - RobotShoot - robot shooting

Game
    - Game - runs during the game cycle
    - GameState - handles the different game states
    - GameUI - updates game user interface


Organizing the project
++++++++++++++++++++++

In this project, we will make 3 independent scenes: ``Player``, ``Mob``, and ``HUD``, which we will combine into the game's ``Main`` scene. In a larger project, it might be useful to create folders to hold the various scenes and their  scripts, but for this relatively small game, you can save your scenes and scripts in the project's root folder identified by ``res://``.  You can see your project folders in the FileSystem Dock in the lower left corner:

