Installation
============

This library requires that two additional libraries be added to your Godot project. The first is the `gs-ecs` library itself. The second, is a logging system called `gs-logging`.

.. note::

    In the instructions below, pick the BRANCH that is most appropriate for your particular project. For example, if you are working on a 3.1.1 project, pick the 3.1 branch. For a 3.0 project, use the 3.0 branch and so on.

Manual Installation
-------------------
- download the package from https://gitlab.com/godot-stuff/gs-ecs
- unzip the folder gs_ecs into your project
- download the package from https://gitlab.com/godot-stuff/gs-logger
- unzip the folder gs_logger into your project


Installation With gs-project-manager
------------------------------------

- add the following entry in the *assets:* section of your **project.yml**

.. code:: yaml

  gs-ecs:
    location: https://gitlab.com/godot-stuff/gs-ecs.git
    branch: 3.2
    includes:
      - dir: gs_ecs

  gs-logger:
    location: https://gitlab.com/godot-stuff/gs-logger.git
    branch: 3.2
    includes:
      - dir: gs_ecs

Installation Using Asset Library
--------------------------------

- open up the Asset Library from Godot and search for godot-stuff
- Download and Install the ECS and Logger libraries

Activate Plugin
---------------
- after it is installed open your Project Settings and Activate the Plugin

