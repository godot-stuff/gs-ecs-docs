.. _Entity:

Entity
======

Inherits: Node_

Entities represent the **things** in your game. It does not have any behavior or data; it does contain components and nodes which are needed for the entity. Systems provide the behavior, while Components contain the data.

Description
+++++++++++

An entity is essentially a Node in Godot. In fact it is a base class of a Godot Node.

In the example below the Entity called **TestEntity** has three **Components** attached to it called **Movable**, **Rotating** and **Energy** respectively. The node called "Components" is used to keep the structure of the Entity node clean and gives a good visual representation for the developer.

.. image:: images/components.png

Properties
++++++++++

+---------------+---------------------------+
|   int_        |   id_                     |
+---------------+---------------------------+
|   bool_       |   enabled_                |
+---------------+---------------------------+

Methods
+++++++

+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_init_ ()                                                                 |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_ready_ ()                                                                |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_before_add_ ()                                                           |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_after_add_ ()                                                            |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_before_remove_ ()                                                        |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_after_remove_ ()                                                         |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_enter_tree_ ()                                                           |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   on_exit_tree_ ()                                                            |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   add_component_ (:ref:`component` component)                                 |
+-----------------------+-------------------------------------------------------------------------------+
|   :ref:`component`    |   get_component_ (String_ name)                                               |
+-----------------------+-------------------------------------------------------------------------------+
|   bool_               |   has_component_ (String_ name)                                               |
+-----------------------+-------------------------------------------------------------------------------+
|   void                |   remove_component_ (String_ name)                                            |
+-----------------------+-------------------------------------------------------------------------------+

Property Descriptions 
+++++++++++++++++++++

.. _id:

* int_ **id**

The internal identifier for the Entity. This is essentially the same as the Godot object id.

.. _enabled:

* bool_ **enabled**

Lets you enable the entity by setting this to True. When an Entity is not enabled when its setting is False, it will not be included during System updates.

Method Descriptions 
+++++++++++++++++++

.. _on_init:

* void **on_init** ()

The framework will make a virtual call when the Entity is initialized. Use this in place of _init().

.. _on_ready:

* void **on_ready** ()

The framework will make this virtual call when the Entity is ready. Use this in place of _ready().

.. _on_before_add:

* void **on_before_add** ()

The framework will make this virtual call when the Entity immediately before it is added to the framework.

.. _on_after_add:

* void **on_after_add** ()

The framework will make this virtual call when the Entity immediately after it has been added to the framework.

.. _on_after_remove:

* void **on_after_remove** ()

The framework will make this virtual call just after the Entity is removed from memory.

.. _on_before_remove:

* void **on_before_remove** ()

The framework will make this virtual call just before the Entity is removed from memory.

.. _on_enter_tree:

* void **on_enter_tree** ()

The framework will make this virtual call when the Entity enters the Tree. Use this in place of _enter_tree().

.. _on_exit_tree:

* void **on_exit_tree** ()

The framework will make this virtual call when the Entity exits the Tree.  Use this in place of _exit_tree().

.. _add_component:

* void **add_component** (:ref:`component` component)

Adds another :ref:`component` to this Entity. If the :ref:`component` has already been added it will not be added again.

.. _get_component:

* void **get_component** (String_ name)

Returns the :ref:`component` based on the Name that it is given. If the :ref:`component` is not found it will return null and a Warning will be logged.

.. _has_component:

* bool_ **has_component** (String_ name)

Returns True if the :ref:`component` `name` does exist for the Entity, otherwise it will return False.

.. _remove_component:

* bool_ **remove_component** (String_ name)

Removes the :ref:`component` in `name`. If the :ref:`component` is not found it will log a Warning.

.. _int: https://docs.godotengine.org/en/stable/classes/class_int.html#class-int
.. _bool: https://docs.godotengine.org/en/stable/classes/class_bool.html#class-bool
.. _node: https://docs.godotengine.org/en/stable/classes/class_node.html?highlight=node
.. _String: https://docs.godotengine.org/en/stable/classes/class_string.html
